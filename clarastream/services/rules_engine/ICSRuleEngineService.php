<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/18/14
 * Time: 12:57 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

/*
 * RulesEngine Service will simply forward a RulesEngine Request to the Services server
 * It is a naive forward at this point, and its the only thing we can do.
*/

interface ICSRulesEngineService extends ICSService
{
    public function createRulesEngineRequest($projectId, $ruleSetId, $masterUserName = null, $masterUserId = null);
 
} 