<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/ICSProductService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSProduct.php');

class CSProductService extends CSServiceBase implements ICSProductService
{
    public function getBaseProducts($type = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/product';

        $queryParams = array();
        if ($type != null)
        {
            $queryParams['type'] = $type;
        }
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $productList = array();
        if (isset($responseArray['products']))
        {
            foreach ($responseArray['products'] as $productData)
            {
                if ($productData != null)
                {
                    array_push($productList, new CSProduct($productData));
                }
            }
        }


        return $productList;
    }

    public function getProduct($productId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/product/' . $productId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['product']) ? new CSProduct($responseArray['product']) : null;
    }

    public function getBaseProductBySKU($productSKU, $masterUserName = null, $masterUserId = null)
    {
        $url = '/product/sku/' . rawurlencode($productSKU);

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['product']) ? new CSProduct($responseArray['product']) : null;
    }

    public function createBaseProduct($product, $mainImageURL = null, $subImageURLs = array(), $masterUserName = null, $masterUserId = null)
    {
        $url = '/product';

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["productData"] = ($product != null) ? $product->getRawData() : array();
        $dataArray["mainImageURL"] = $mainImageURL;
        $dataArray["subImageURLs"] = ($subImageURLs != null && is_array($subImageURLs)) ? $subImageURLs : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['product']) ? new CSProduct($responseArray['product']) : null;
    }

    public function getBaseProductVariations($offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/variation';

        $queryParams = array();
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $productList = array();
        if (isset($responseArray['products']))
        {
            foreach ($responseArray['products'] as $productData)
            {
                if ($productData != null)
                {
                    array_push($productList, new CSProduct($productData));
                }
            }
        }


        return $productList;
    }
}