<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/18/14
 * Time: 12:55 PM
 */

require_once(dirname(__FILE__) . '/ICSServiceAuthDelegate.php');
require_once(dirname(__FILE__) . '/queue/CSQueueService.php');
require_once(dirname(__FILE__) . '/sync/CSSyncService.php');
require_once(dirname(__FILE__) . '/user/CSUserService.php');
require_once(dirname(__FILE__) . '/template/CSTemplateService.php');
require_once(dirname(__FILE__) . '/project/CSProjectService.php');
require_once(dirname(__FILE__) . '/product/CSProductService.php');
require_once(dirname(__FILE__) . '/category/CSCategoryService.php');
require_once(dirname(__FILE__) . '/rules_engine/CSRulesEngineService.php');
require_once(dirname(__FILE__) . '/plugin/CSPluginService.php');

class ClaraStreamServiceFactory
{
    const CONFIG_SECTION_CONNECTION = "service.connection";
    const CONFIG_VAR_CONNECTION_BASE_SERVICE_URL = "service.baseServiceURL";
    const CONFIG_VAR_CONNECTION_USERNAME = "service.username";
    const CONFIG_VAR_CONNECTION_PASSWORD = "service.password";

    const ENV_VAR_CONNECTION_BASE_SERVICE_URL = "CLARASTREAM_SERVICES_URL";
    const ENV_VAR_CONNECTION_USERNAME = "CLARASTREAM_SERVICES_USERNAME";
    const ENV_VAR_CONNECTION_PASSWORD = "CLARASTREAM_SERVICES_PASSWORD";

    const DEFAULT_BASE_SERVICE_URL = "https://services.clarastream.com";

    private static $_isInitialized = false;
    private static $_baseServiceURL = null;
    private static $_serviceUsername = null;
    private static $_servicePassword = null;
    private static $_serviceAuthDelegate = null;

    private static $_queueService = null;
    private static $_syncService = null;
    private static $_userService = null;
    private static $_templateService = null;
    private static $_projectService = null;
    private static $_productService = null;
    private static $_categoryService = null;
    private static $_ruleService = null;
    private static $_pluginService = null;

    public static function initialize()
    {
        if (!self::$_isInitialized)
        {
            self::$_isInitialized = true;

            //  Determine services URL based on the following precedence:
            //  1:  Environment variable
            //  2:  Config file setting
            //  3:  Default value

            //  Process config file...
            $ini_array = parse_ini_file(dirname(__FILE__) . '/../../config.ini');

            $tempBaseServiceURL = self::determineInitializationValue(self::$_baseServiceURL, self::ENV_VAR_CONNECTION_BASE_SERVICE_URL, self::CONFIG_VAR_CONNECTION_BASE_SERVICE_URL, $ini_array);
            $tempServiceUsername = self::determineInitializationValue(self::$_serviceUsername , self::ENV_VAR_CONNECTION_USERNAME, self::CONFIG_VAR_CONNECTION_USERNAME, $ini_array);
            $tempServicePassword = self::determineInitializationValue(self::$_servicePassword, self::ENV_VAR_CONNECTION_PASSWORD, self::CONFIG_VAR_CONNECTION_PASSWORD, $ini_array);

            if ($tempBaseServiceURL && strlen($tempBaseServiceURL) > 0)
            {
                self::$_baseServiceURL = $tempBaseServiceURL;
            }
            else
            {
                self::$_baseServiceURL = ClaraStreamServiceFactory::DEFAULT_BASE_SERVICE_URL;
            }

            if ($tempServiceUsername && strlen($tempServiceUsername) > 0)
            {
                self::$_serviceUsername = $tempServiceUsername;
            }

            if ($tempServicePassword && strlen($tempServicePassword) > 0)
            {
                self::$_servicePassword = $tempServicePassword;
            }

            error_log("ClaraStreamServiceFactory:  Using services URL: " . self::$_baseServiceURL);
            error_log("ClaraStreamServiceFactory:  Using services username: " . self::$_serviceUsername);
            //error_log("ClaraStreamServiceFactory:  Using services password: " . self::$_servicePassword);

            self::$_serviceAuthDelegate = new ServiceAuthDelegate();
            if (self::$_serviceUsername && strlen(self::$_serviceUsername) > 0 && self::$_servicePassword && strlen(self::$_servicePassword) > 0)
            {
                self::$_serviceAuthDelegate->authenticate(self::$_serviceUsername, self::$_servicePassword);
            }
        }
    }

    private static function determineInitializationValue($currentValue, $envVariableName, $configVariableName, $ini_array)
    {
        $value = $currentValue;

        if ($value == null || strlen($value) <= 0)
        {
            $value = getenv($envVariableName);
        }

        if ($value == null || strlen($value) <= 0 && isset($ini_array[$configVariableName]))
        {
            $value = $ini_array[$configVariableName];
        }

        return $value;
    }

    public static function setBaseServiceURL($baseServiceURL)
    {
        self::$_baseServiceURL = $baseServiceURL;
    }

    public static function getBaseServiceURL()
    {
        return self::$_baseServiceURL;
    }

    public static function authenticate($username, $password)
    {
        self::$_serviceUsername = $username;
        self::$_servicePassword = $password;

        self::initialize();

        error_log("ClaraStreamServiceFactory->authenticate():  Using services URL: " . self::$_baseServiceURL);
        error_log("ClaraStreamServiceFactory->authenticate():  Using services username: " . self::$_serviceUsername);
        //error_log("ClaraStreamServiceFactory->authenticate():  Using services password: " . self::$_servicePassword);

        self::$_serviceAuthDelegate->authenticate($username, $password);
    }

    public static function resetAuthentication()
    {
        self::initialize();

        self::$_serviceAuthDelegate->resetAuthentication();
    }

    public static function getQueueService()
    {
        self::initialize();

        if (self::$_queueService == null)
        {
            self::$_queueService = new CSQueueService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_queueService;
    }

    public static function getSyncService()
    {
        self::initialize();

        if (self::$_syncService == null)
        {
            self::$_syncService = new CSSyncService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_syncService;
    }

    public static function getUserService()
    {
        self::initialize();

        if (self::$_userService == null)
        {
            self::$_userService = new CSUserService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_userService;
    }

    public static function getTemplateService()
    {
        self::initialize();

        if (self::$_templateService == null)
        {
            self::$_templateService = new CSTemplateService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_templateService;
    }

    public static function getProjectService()
    {
        self::initialize();

        if (self::$_projectService == null)
        {
            self::$_projectService = new CSProjectService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_projectService;
    }

    public static function getProductService()
    {
        self::initialize();

        if (self::$_productService == null)
        {
            self::$_productService = new CSProductService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_productService;
    }

    public static function getPluginService()
    {
        self::initialize();

        if (self::$_pluginService == null)
        {
            self::$_pluginService = new CSPluginService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_pluginService;
    }



    public static function getCategoryService()
    {
        self::initialize();

        if (self::$_categoryService == null)
        {
            self::$_categoryService = new CSCategoryService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_categoryService;
    }

    public static function getRuleService()
    {
        self::initialize();

        if (self::$_ruleService == null)
        {
            self::$_ruleService = new CSRulesEngineService(self::$_baseServiceURL, self::$_serviceAuthDelegate);
        }

        return self::$_ruleService;
    }

}

class ServiceAuthDelegate implements ICSServiceAuthDelegate
{
    private $_username = null;
    private $_password = null;

    public function resetAuthentication()
    {
        $this->_username = null;
        $this->_password = null;
    }

    public function authenticate($username, $password)
    {
        $this->_username = $username;
        $this->_password = $password;
    }

    public function configureGuzzleAuthentication($guzzleRequest)
    {
        if ($guzzleRequest != null && $this->_username != null && strlen($this->_username != null) > 0 && $this->_password != null && strlen($this->_password != null) > 0)
        {
            error_log("ClaraStreamServiceFactory::ServiceAuthDelegate::configureGuzzleAuthentication(): configuring user authentication: username = $this->_username");
            $guzzleRequest->setAuth($this->_username, $this->_password);
        }
    }
}