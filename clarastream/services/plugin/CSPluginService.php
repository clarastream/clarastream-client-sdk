<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/24/14
 * Time: 9:51 PM
 */

require_once(dirname(__FILE__) . '/ICSPluginService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSPlugin.php');

class CSPluginService extends CSServiceBase implements ICSPluginService
{
    public function getVersions($type, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/plugin/' . $type;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        $pluginList = array();
        if (isset($responseArray['plugins']))
        {
            foreach ($responseArray['plugins'] as $in)
            {
                if ($in != null)
                {
                    array_push($pluginList, new CSPlugin($in));
                }
            }
        }
        return $pluginList;
    }

    public function getPluginUrlInfo($id, $masterUserName = null, $masterUserId = null)
    {
    	$url = parent::getBaseServiceURL() . '/plugin/link/' . $id;
    	
    	$queryParams = array();
    	if ($masterUserName != null)
    	{
    		$queryParams['masterUserName'] = "" . $masterUserName;
    	}
    	if ($masterUserId != null)
    	{
    		$queryParams['masterUserId'] = "" . $masterUserId;
    	}
    	
    	$responseArray = $this->processCallToURL($url, $queryParams, null, "GET");
    	
    	$plugin = isset($responseArray['plugin']) ? new CSPlugin($responseArray['plugin']) : null;
    	return $plugin;
    }
    
    public function getPluginFromUrlInfo($urlplugin,$toFile)
    {
    	if(!is_null($urlplugin))
    	{
    		return $urlplugin->getPlugin($toFile);
    	}
    }
    
    public function getPlugin($id, $toFile, $masterUserName = null, $masterUserId = null)
    {
      	
         $urlplugin = $this->getPluginUrlInfo($id,$masterUserName,$masterUserId);
		 return $this->getPluginFromUrlInfo($urlplugin, $tofile);
    }
}
