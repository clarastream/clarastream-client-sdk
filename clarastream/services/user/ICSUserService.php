<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/19/14
 * Time: 1:19 AM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSUserService extends ICSService
{
    public function getUserByUsername($username, $masterUserName = null, $masterUserId = null);
}