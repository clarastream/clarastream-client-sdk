<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/18/14
 * Time: 12:57 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSSyncService extends ICSService
{
    public function syncProjectSKUs($projectId, $cartSyncInfoList, $masterUserName = null, $masterUserId = null);
    public function getProjectSyncData($projectId, $masterUserName = null, $masterUserId = null);
    public function removeProductFromProjectSyncData($projectId, $productId, $masterUserName = null, $masterUserId = null);
    public function getProjectSKUSyncStatus($token, $masterUserName = null, $masterUserId = null);
    public function addProjectSKUSyncStatus($token, $objectId, $total, $masterUserName = null, $masterUserId = null);
    public function updateProjectSKUSyncStatus($token, $inserted, $masterUserName = null, $masterUserId = null);
    public function completeProjectSKUSyncStatus($token, $inserted, $masterUserName = null, $masterUserId = null);
} 