<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/18/14
 * Time: 12:59 PM
 */

require_once(dirname(__FILE__) . '/ICSSyncService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSPolling.php');
require_once(dirname(__FILE__) . '/../../models/CSSyncData.php');

class CSSyncService extends CSServiceBase implements ICSSyncService
{
    public function syncProjectSKUs($projectId, $cartSyncInfoList, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/sync/project/' . $projectId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array("cartSyncInfoList" => $cartSyncInfoList);
        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray);

        $tokenArray = $responseArray['tokenArray'];

        error_log("CSSyncService::syncProjectSKUs: tokenArray = " . $tokenArray);

        return $tokenArray;
    }

    public function getProjectSyncData($projectId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/sync/project/syncData/' . $projectId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['syncData']) ? new CSSyncData($responseArray['syncData']) : null;
    }

    public function removeProductFromProjectSyncData($projectId, $productId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/sync/project/syncData/' . $projectId . "/remove/" . $productId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "PUT");

        // TODO (WK) Determine what to do with error situations...

        return;
    }

    public function getProjectSKUSyncStatus($token, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/sync/project/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        return isset($responseArray['status']) ? new CSPolling($responseArray['status']) : null;
    }

    public function addProjectSKUSyncStatus($token, $objectId, $total, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/sync/project/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array(
            "objectId" => $objectId,
            "total" => $total);

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray);

        return isset($responseArray['status']) ? new CSPolling($responseArray['status']) : null;
    }

    public function updateProjectSKUSyncStatus($token, $inserted, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/sync/project/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array("inserted" => $inserted);

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "PUT");

        return;
    }

    public function completeProjectSKUSyncStatus($token, $inserted, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/sync/project/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array(
            "status" => "jc",
            "inserted" => $inserted);

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "PUT");

        return;
    }
} 