<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 12:59 PM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');
require_once(dirname(__FILE__) . '/CSCategory.php');

class CSProduct extends CSAbstractModel
{

    private $_categories = null;

    public function getSKU()
    {
        return isset($this->_rawData['sku']) ? $this->_rawData['sku'] : null;
    }

    public function setSKU($sku)
    {
        $this->_rawData['sku'] = $sku;
    }

    public function getType()
    {
        return isset($this->_rawData['type']) ? $this->_rawData['type'] : null;
    }

    public function getName()
    {
        return isset($this->_rawData['name']) ? $this->_rawData['name'] : null;
    }

    public function setName($name)
    {
        $this->_rawData['name'] = $name;
    }

    public function getProjectId()
    {
        return isset($this->_rawData['projectId']) ? $this->_rawData['projectId'] : null;
    }

    public function setProjectId($projectId)
    {
        $this->_rawData['projectId'] = $projectId;
    }

    public function getUserId()
    {
        return isset($this->_rawData['userId']) ? $this->_rawData['userId'] : null;
    }

    public function getVariation()
    {
        return isset($this->_rawData['variation']) ? $this->_rawData['variation'] : null;
    }

    public function getImageMain()
    {
        return isset($this->_rawData['imageMain']) ? $this->_rawData['imageMain'] : null;
    }

    public function getImages()
    {
        return isset($this->_rawData['images']) ? $this->_rawData['images'] : null;
    }

    public function getCategories()
    {
        if ($this->_categories == null)
        {
            $this->_categories = array();
            if (isset($this->_rawData['categories']) && is_array($this->_rawData['categories']))
            {
                foreach ($this->_rawData['categories'] as $categoryData)
                {
                    array_push($this->_categories, new CSCategory($categoryData));
                }
            }
        }
        return $this->_categories;
    }

    public function addCategory(/* CSCategory */ $category)
    {
        // Reset category cache to trigger refresh
        $this->_categories = null;

        if (! (isset($this->_rawData['categories']) && is_array($this->_rawData['categories'])))
        {
            $this->_rawData['categories'] = array();
        }

        array_push($this->_rawData['categories'], $category->getRawData());
    }

    public function getCost()
    {
        return isset($this->_rawData['cost']) ? $this->_rawData['cost'] : null;
    }

    public function setCost($cost)
    {
        $this->_rawData['cost'] = $cost;
    }

    public function getPrice()
    {
        return isset($this->_rawData['price']) ? $this->_rawData['price'] : null;
    }

    public function setPrice($price)
    {
        $this->_rawData['price'] = $price;
    }

    public function getMSRP()
    {
        return isset($this->_rawData['msrp']) ? $this->_rawData['msrp'] : null;
    }

    public function setMSRP($msrp)
    {
        $this->_rawData['msrp'] = $msrp;
    }

    public function getDescription()
    {
        return isset($this->_rawData['description']) ? $this->_rawData['description'] : null;
    }

    public function setDescription($description)
    {
        $this->_rawData['description'] = $description;
    }

    public function getShortDescription()
    {
        return isset($this->_rawData['shortDescription']) ? $this->_rawData['shortDescription'] : null;
    }

    public function setShortDescription($shortDescription)
    {
        $this->_rawData['shortDescription'] = $shortDescription;
    }

    public function getStatus()
    {
        return isset($this->_rawData['status']) ? $this->_rawData['status'] : null;
    }

    public function setStatus($status)
    {
        $this->_rawData['status'] = $status;
    }

    public function getAlternateSKU()
    {
        return isset($this->_rawData['alternateSKU']) ? $this->_rawData['alternateSKU'] : null;
    }

    public function setAlternateSKU($alternateSKU)
    {
        $this->_rawData['alternateSKU'] = $alternateSKU;
    }

    // TODO: (BT) Convert Bundle to an Object, and move translation to Data Ingestion
    public function getLinksBundles()
    {
        $extraOptions = $this->getExtraOptions();

        $bundle = new StdClass();

        // make all the keys as a string for faster searching
        $keystring = implode(', ', array_keys($extraOptions));
        preg_match_all('/links_bundle_name_\d+/', $keystring, $matches);
        // preg match all returns a multi-dimensional array
        $matches = $matches[0];
        $bundledSelected = array();

        // go through each bundle selection name and add all the skus
        foreach ($matches as $name)
        {

            $bundle = new StdClass();
            // use the number of the links bundle as the index
            $i = (int) array_pop(explode('_', $name));

            $links_bundle = 'links_bundle_skus_' . $i;
            if (!empty($extraOptions[$links_bundle]))
            {
                $bundle->name = $extraOptions[$name];

                // set the type
                $bundle_type = 'links_bundle_type_' . $i;
                $bundle->type = isset($extraOptions[$bundle_type]) && !empty($extraOptions[$bundle_type]) ?
                    $extraOptions[$bundle_type] : 'select';
                $bundle->entries = array();

                // set the sku level options
                $links_qty = 'links_bundle_qty_' . $i;
                $c = 0;
                $extraOptions[$links_qty] = (array) $extraOptions[$links_qty];
                foreach ( (array) $extraOptions[$links_bundle] as $s)
                {
                    $sku = new StdClass();
                    $sku->sku = $s;

                    $sku->qty = isset($extraOptions[$links_qty][$c]) && $extraOptions[$links_qty][$c] != ''
                       ? $extraOptions[$links_qty][$c] :  1;

                    $c++;
                    $bundle->entries[] = $sku;
                }
            }
            $i++;
            $bundledSelected[] = $bundle;
        }
        return $bundledSelected;
    }

    public function getExtraOptions()
    {
        return isset($this->_rawData['extraOptions']) ? $this->_rawData['extraOptions'] : null;
    }
}
