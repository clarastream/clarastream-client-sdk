<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 11:24 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSPolling extends CSAbstractModel
{
    public function getObjectId()
    {
        return isset($this->_rawData['objectId']) ? $this->_rawData['objectId'] : null;
    }

    public function getTotal()
    {
        return isset($this->_rawData['total']) ? $this->_rawData['total'] : 0;
    }

    public function getInserted()
    {
        return isset($this->_rawData['inserted']) ? $this->_rawData['inserted'] : 0;
    }

    public function getStatus()
    {
        return isset($this->_rawData['status']) ? $this->_rawData['status'] : null;
    }
}