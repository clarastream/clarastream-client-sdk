<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/19/14
 * Time: 1:34 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSPlugin extends CSAbstractModel
{
    public function getType()
    {
        return isset($this->_rawData['type']) ? $this->_rawData['type'] : null;
    }

    public function getVersion()
    {
        return isset($this->_rawData['version']) ? $this->_rawData['version'] : null;
    }

    public function getDescription()
    {
        return isset($this->_rawData['description']) ? $this->_rawData['description'] : null;
    }

    public function getPlugin($toFile){
        try {
            error_log("Initiating plugin download of: " . $this->getUrl());
            error_log("Trying to save to to: " . $toFile);
            $client = new Guzzle\Http\Client();
            $response = $client->get($this->getUrl())
                //->setAuth('login', 'password') // in case your resource is under protection
                ->setResponseBody($toFile)
                ->send();

            return $this->_verifyDownload($toFile);

        } catch (Exception $e) {
            error_log("Download failed: " . $e->getMessage());
            return false;
        }
    }

    public function getUrl()
    {
        return isset($this->_rawData['url']) ? $this->_rawData['url'] : null;
    }

    private function _getMD5()
    {
        return isset($this->_rawData['md5']) ? $this->_rawData['md5'] : null;
    }


    private function _verifyDownload($toFile)
    {
        if(file_exists($toFile) && md5_file($toFile) == $this->_getMD5())
            return true;
        else
            return false;
    }
}
