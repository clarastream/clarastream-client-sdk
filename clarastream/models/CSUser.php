<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/19/14
 * Time: 1:34 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSUser extends CSAbstractModel
{
    public function getUsername()
    {
        return isset($this->_rawData['username']) ? $this->_rawData['username'] : null;
    }
}